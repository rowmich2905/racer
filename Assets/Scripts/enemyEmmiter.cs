﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyEmmiter : MonoBehaviour
{
    float nextLaunchTime;//время следующего запуска
    float enemyAcceleration = 10f;
    float halfsize, secondhalfsize;
    public GameObject enemy;
    public float EminDelay;//минимальная задержка запуска врага
    public float EmaxDelay;//максимальная задержка запуска врага
    Vector3 pos;

    // Start is called before the first frame update
    void Start()
    {
        halfsize = transform.localScale.x / 2;//вычисление границ спавна по X
        secondhalfsize = transform.localScale.y / 2;//вычисление границ спавна по Y
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextLaunchTime) //запускаем объект если время больше времени запуска
        {
            pos = new Vector3(Random.Range(-halfsize, halfsize), Random.Range(-secondhalfsize, secondhalfsize), 400);//рассчет позиции спавна
            Instantiate(enemy, pos, Quaternion.identity);
            nextLaunchTime = Time.time + Random.Range(EminDelay, EmaxDelay) / enemyAcceleration;
            enemyAcceleration += 0.05f;
        }
    }
}
