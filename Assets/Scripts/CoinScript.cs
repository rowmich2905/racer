﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinScript : MonoBehaviour
{
     GameObject coinScore;//текст коинов
    int temp;// для ведения счета
    public float speed;
    private GameObject player;
    Rigidbody coin;
    // Start is called before the first frame update
    void Start()
    {
        coin = GetComponent<Rigidbody>();
        coin.velocity = new Vector3(0, 0, -speed);
        player = GameObject.FindGameObjectsWithTag("Player")[0];
        coinScore = GameObject.Find("coinCount");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "shild")
        {
            int.TryParse(coinScore.GetComponent<Text>().text, out temp); //перевод из формата текст в int  
            temp+=100;// очки
            coinScore.GetComponent<Text>().text = temp.ToString();//запись очков в текст
            Destroy(gameObject);
        
        }
    }
}
