﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    GameObject score;
    int temp;
    // Start is called before the first frame update
    void Start()
    {
        score = GameObject.Find("Score");
    }

    // Update is called once per frame
    //void Update()
    //{

    //}
    public void AddScore(int addition)
    {
        int.TryParse(score.GetComponent<Text>().text, out temp); //перевод из формата текст в int  
        temp += addition;// очки
        score.GetComponent<Text>().text = temp.ToString();//запись очков в текст
    }
}
