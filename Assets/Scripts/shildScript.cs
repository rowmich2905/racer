﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shildScript : MonoBehaviour
{
    public float speed;
    private GameObject player;
    Rigidbody shild;


    // Start is called before the first frame update
    void Start()
    {
        shild = GetComponent<Rigidbody>();
        shild.velocity = new Vector3(0, 0, -speed);
        player = GameObject.FindGameObjectsWithTag("Player")[0];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Destroy(gameObject);
        }
    }
}
