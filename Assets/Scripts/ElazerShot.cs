﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElazerShot : MonoBehaviour
{
    public float speed;// скорость вылета
    private GameObject player;//объект в который стреляем
    private Vector3 dist;
    void Start()
    {
        // GetComponent<Rigidbody>().velocity = new Vector3(0, 0, speed);// выстрел
        player = GameObject.FindGameObjectsWithTag("Player")[0];// нашли объект в который стреляем 
        dist = player.transform.position - transform.position;
        GetComponent<Rigidbody>().velocity = dist;
        transform.LookAt(player.transform);//слежение за целью
    }

    // Update is called once per frame
    //void Update()
    //{
        
        
    //    //if (GameController.isPaused)
    //    //{
    //    //    return;
    //    //}
    //}
}
