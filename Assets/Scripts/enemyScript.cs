﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyScript : MonoBehaviour
{
    public GameObject asteroidExp;
    public GameObject laserShot;//снаряд для выстрела
    public GameObject laserGun;//пущщка
    public float speed;//скорость корабля
    public float shotDelay;//задержка между выстрелами 
    float nextShotTime = 0;//время когда можно делать следующий выстрел
    public GameObject player;//пееменные для поиска дистанции dist
    public GameObject enemy;
    public Transform target;//цель для слежения
    Rigidbody ship;
    private float dist;
    GameObject scoreKeeper;

    void Start()
    {
        ship = GetComponent<Rigidbody>(); //получить компонент
        ship.velocity = new Vector3 (0, 0, -speed);// движение вперед 
        scoreKeeper = GameObject.Find("ScoreKeeper");
    }

    void Update()
    {
        dist = Vector3.Distance(enemy.transform.position, player.transform.position); //посчитать дистанцию между объектами 
        transform.LookAt(target);//слежение за целью
        if (Time.time > nextShotTime && dist <= 50f)//проверка на возможность выстрела 
        {
            Instantiate(laserShot, laserGun.transform.position, Quaternion.identity);//создать лазерный выстрел(что создаём, откуда вылетает, поворот)
            nextShotTime = Time.time + shotDelay;
        }
        if (dist <= 20f) 
        {
            ship.velocity = new Vector3(0, 0, 0);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "boundary")
        {
            return;
        }
        if (other.tag == "shild" || other.tag == "Laser")
        {
            scoreKeeper.GetComponent<Score>().AddScore(100);
        }
            Instantiate(asteroidExp, transform.position, Quaternion.identity);
        Destroy(other.gameObject);//уничтожить столкнувшийся объект
        Destroy(gameObject);//уничтожить себя
           
    }

}
