﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainButton : MonoBehaviour
{
    public void MainMenu() => SceneManager.LoadScene("Main");
}
