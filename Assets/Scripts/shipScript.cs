﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shipScript : MonoBehaviour
{
    public GameObject shildPosition;//позиция щита 
    public GameObject shild;//щит 
    public GameObject asteroidExp;
    public GameObject laserShot;//снаряд для выстрела
    public GameObject laserGun;//пущщка
    public float speed;//скорость корабля
    public float tild;//наклон корабля
    public float xMin, xMax, yMin, yMax;//границы перемещения
    public float shotDelay;//задержка между выстрелами 
    float nextShotTime = 0;//время когда можно делать следующий выстрел
    Rigidbody ship;
    [SerializeField] private bl_Joystick Joystick;


    void Start()
    {
        ship = GetComponent<Rigidbody>(); //получить компонент
    }

    // обновлнение при каждом кадре
    void Update()
    {
        float h = Joystick.Horizontal;//вправо-влево
        float v = Joystick.Vertical;// вверх-вниз
        ship.velocity = new Vector3(h, v, 0) * speed;//спросчет скорости по координатам(x,y,z)
                                                                             //Если позиция по оси х в рамках, то мы её не меняем
                                                                             //Если позиция выходит за рамки то мы её меняем на максимальное зачение
        float correctx = Mathf.Clamp(ship.position.x, xMin, xMax);//проверяем раницы по х
        float correcty = Mathf.Clamp(ship.position.y, yMin, yMax);//проверяем границы по y
        ship.position = new Vector3(correctx, correcty, 0);
        ship.rotation = Quaternion.Euler(-ship.velocity.y * tild, 0, -ship.velocity.x * tild);//для поворота

        //if (Time.time>nextShotTime && Input.GetButton("Fire2")) //проверка на возможность выстрела 
        //{
        //    Instantiate(laserShot, laserGun.transform.position, Quaternion.identity);//создать лазерный выстрел(что создаём, откуда вылетает, поворот)
        //    nextShotTime = Time.time + shotDelay;
        //}
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "bonus")
        {
            Instantiate(shild, shildPosition.transform.position, Quaternion.identity);
        }
        if (other.tag == "coin") 
        {
            return;
        }
        if (other.tag !="bonus")
        {
            Instantiate(asteroidExp, transform.position, Quaternion.identity);
            Destroy(other.gameObject);//уничтожить столкнувшийся объект
            Destroy(gameObject);//уничтожить астероид
            
        }    
    }
   public void  OnClick()
    {
        if (Time.time > nextShotTime)
        {
            Instantiate(laserShot, laserGun.transform.position, Quaternion.identity);//создать лазерный выстрел(что создаём, откуда вылетает, поворот)
            nextShotTime = Time.time + shotDelay;
        }
    }
}
