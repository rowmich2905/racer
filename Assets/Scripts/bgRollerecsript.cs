﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bgRollerecsript : MonoBehaviour
{
    public GameObject skybox;
    public float rotationSpeed;
    // Start is called before the first frame update
    void Start()
    {
        Rigidbody asteroid = GetComponent<Rigidbody>();
        asteroid.angularVelocity = Random.insideUnitSphere * rotationSpeed;
    }

}
