﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtectionField : MonoBehaviour
{
    public float timer;
    public Transform target;
    public GameObject Target;
    public float smoothTime = 0.1F;
    private Vector3 velocity = Vector3.zero;
    void Start()
    {
        Target = GameObject.FindGameObjectsWithTag("ShildEmmiter")[0];
    }

    void Update()
    {
        Vector3 targetPosition = new Vector3(Target.transform.position.x, Target.transform.position.y, Target.transform.position.z);
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        if (timer < 20f) 
        {
            timer += Time.deltaTime;
        }
        if (timer > 20f) 
        {
            Destroy(gameObject);
        }
    

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag =="Laser")
        {
            return;
        }
        Destroy(other.gameObject);//уничтожить столкнувшийся объект
    }
}
