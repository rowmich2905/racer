﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bonusEmmiter : MonoBehaviour
{
    float nextLaunchTime;//время следующего запуска
    float bonusAcceleration = 5f;
    float halfsize, secondhalfsize;
    public GameObject bonus;
    //public GameObject coin;
    public float EminDelay;//минимальная задержка запуска врага
    public float EmaxDelay;//максимальная задержка запуска врага
    Vector3 pos;

    // Start is called before the first frame update
    void Start()
    {
        halfsize = transform.localScale.x / 2;//вычисление границ спавна по X
        secondhalfsize = transform.localScale.y / 2;//вычисление границ спавна по Y
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextLaunchTime) //запускаем объект если время больше времени запуска
        {
            //pos = new Vector3(Random.Range(-halfsize, halfsize), Random.Range(-secondhalfsize, secondhalfsize), 450);//рассчет позиции спавна
            //Instantiate(coin, pos, Quaternion.identity);
            //nextLaunchTime = Time.time + Random.Range(EminDelay, EmaxDelay) / bonusAcceleration;

            pos = new Vector3(Random.Range(-halfsize, halfsize), Random.Range(-secondhalfsize, secondhalfsize), 450);//рассчет позиции спавна
            Instantiate(bonus, pos, Quaternion.identity);
            nextLaunchTime = Time.time + Random.Range(EminDelay, EmaxDelay) / bonusAcceleration;
        }
    }
}
