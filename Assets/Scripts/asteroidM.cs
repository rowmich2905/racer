﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroidM : MonoBehaviour
{
    public GameObject asteroid;
    public float rotationSpeed;
    // Start is called before the first frame update
    void Start()
    {
        //рандомное кручение вокруг своей оси 
        Rigidbody asteroid = GetComponent<Rigidbody>();
        asteroid.angularVelocity = Random.insideUnitSphere * rotationSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
