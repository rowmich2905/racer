﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroidEmmiter : MonoBehaviour
{
    public Canvas canvas;
    public Transform pauseMenu;
    public GameObject asteroidS;//запускаемый объект
    public GameObject asteroid;
    public float maxDelay;//максимальная задержка запуска
    public float minDelay;//минимальная задержка запуска 
    float nextLaunchTime;//время следующего запуска 
    float acceleration = 3f;//частота запуска объекта 
    Vector3 pos;
    float halfsize, secondhalfsize;

    void Start()
    {
        halfsize = transform.localScale.x / 2;//вычисление границ спавна по X
        secondhalfsize = transform.localScale.y / 2;//вычисление границ спавна по Y
        pauseMenu = canvas.transform.Find("pauseMenu");
        pauseMenu.gameObject.SetActive(false);
    }

    void Update()
    {

        // pos = new Vector3(Random.Range(-halfsize, halfsize), Random.Range(-secondhalfsize, secondhalfsize), 500);//рассчет позиции спавна
        if (Time.time > nextLaunchTime) //запускаем объект если время больше времени запуска 
        {
            pos = new Vector3(Random.Range(-halfsize, halfsize), Random.Range(-secondhalfsize, secondhalfsize), 500);//рассчет позиции спавна
            Instantiate(asteroid, pos, Quaternion.identity);//создание объекта 
            nextLaunchTime = Time.time + Random.Range(minDelay, maxDelay) / acceleration;//появление объекта с ускорением частоты появления 
            acceleration += 0.005f;//ускорение частоты появления 
            pos = new Vector3(Random.Range(-halfsize, halfsize), Random.Range(-secondhalfsize, secondhalfsize), 500);//рассчет позиции спавна
            Instantiate(asteroidS, pos, Quaternion.identity);
            nextLaunchTime = Time.time + Random.Range(minDelay, maxDelay) / acceleration;
            acceleration += 0.0005f;
        }
        if (GameObject.Find("player") == null)
        {
           
            pauseMenu.gameObject.SetActive(true);
        }
    }
}
