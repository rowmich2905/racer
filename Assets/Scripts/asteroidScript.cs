﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroidScript : MonoBehaviour
{
    public GameObject asteroidExp;//еффект взрыва астероида
    public GameObject playerExp;//еффект взрыва корабля
    public float rotationSpeed;//значение ротации астероида
    public float speed;//значение скорости полёта астероида
    float size;//размер астероида в %
    public float driftX;//снос астероида по оси X
    public float driftY;//снос астероида по оси Y
    Rigidbody asteroid;//назначение компонента что бы астероид двигался
    GameObject scoreKeeper;
    void Start()
    {
        float speedX = Random.Range(-driftX, driftX);
        float speedY = Random.Range(-driftY, driftY);
        size = Random.Range(0.5f, 1.0f);
        asteroid = GetComponent<Rigidbody>();
        asteroid.transform.localScale *= size;//меняем размер
        asteroid.velocity = new Vector3(speedX, speedY, -speed) / size;//скорость астероида 
        asteroid.velocity = new Vector3(0, 0, -speed);//полёт астероида
        asteroid.angularVelocity = Random.insideUnitSphere * rotationSpeed;//вращение астероида
        scoreKeeper = GameObject.Find("ScoreKeeper");
    }

    private void OnTriggerEnter(Collider other)//вызывается в момент начала столкновения
    {
        if (other.tag == "coin")
        {
            return;
        }
        if (other.tag == "bonus")
        {
            return;
        }
        if (other.tag == "boundary")
        {
            return;
        }
        if (other.tag == "shild" || other.tag == "Laser")
        {
            scoreKeeper.GetComponent<Score>().AddScore(10);
            //Instantiate(asteroidExp, transform.position, Quaternion.identity);
            //Destroy(gameObject);//уничтожить астероид
        }
        if (other.tag == "Asteroid")
        {
            asteroid.velocity = new Vector3(Random.Range(-speed, speed), 0, Random.Range(-speed, speed));//разлёт астероидов при столкновении 
        }

        Instantiate(asteroidExp, transform.position, Quaternion.identity);
        //Destroy(other.gameObject);//уничтожить столкнувшийся объект
        Destroy(gameObject);//уничтожить астероид
    }
    void Update()
    {
        
    }
}
